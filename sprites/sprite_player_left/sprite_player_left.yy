{
    "id": "ca2ec2b7-eed6-4d51-a1d6-27fcacacde81",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d81061a-2816-4547-8dd6-e1f8ecb49219",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca2ec2b7-eed6-4d51-a1d6-27fcacacde81",
            "compositeImage": {
                "id": "937b25fb-d0bd-4083-af2b-714cf6056d1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d81061a-2816-4547-8dd6-e1f8ecb49219",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5eba115-a9a7-4602-8d0c-44bfad8ef2f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d81061a-2816-4547-8dd6-e1f8ecb49219",
                    "LayerId": "b4f94f12-e47b-433e-9d07-66dc7e8038b8"
                }
            ]
        },
        {
            "id": "c80fb702-4cc4-47e2-b430-d5d00bb59499",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca2ec2b7-eed6-4d51-a1d6-27fcacacde81",
            "compositeImage": {
                "id": "481dd4d0-bb81-41e7-8017-15520e4356a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c80fb702-4cc4-47e2-b430-d5d00bb59499",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16e1ca91-be00-4270-9052-f77d574eeb08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c80fb702-4cc4-47e2-b430-d5d00bb59499",
                    "LayerId": "b4f94f12-e47b-433e-9d07-66dc7e8038b8"
                }
            ]
        },
        {
            "id": "2a8d5703-408b-4543-973a-c60cc130cb5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca2ec2b7-eed6-4d51-a1d6-27fcacacde81",
            "compositeImage": {
                "id": "052fc207-b791-4acd-b2d2-9e499a0b440d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a8d5703-408b-4543-973a-c60cc130cb5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9debf4bf-49e8-4ed9-b996-77c8458098e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a8d5703-408b-4543-973a-c60cc130cb5b",
                    "LayerId": "b4f94f12-e47b-433e-9d07-66dc7e8038b8"
                }
            ]
        },
        {
            "id": "6f70a69e-0249-40da-99e2-2c0b0a41e8fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca2ec2b7-eed6-4d51-a1d6-27fcacacde81",
            "compositeImage": {
                "id": "ddc75a01-b759-4026-8d45-7896f9e65457",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f70a69e-0249-40da-99e2-2c0b0a41e8fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d61617ca-48a0-4b8e-b99e-e420a5d27798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f70a69e-0249-40da-99e2-2c0b0a41e8fe",
                    "LayerId": "b4f94f12-e47b-433e-9d07-66dc7e8038b8"
                }
            ]
        },
        {
            "id": "c02cdd13-d29f-4c4b-bbf1-cc2e476c0478",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca2ec2b7-eed6-4d51-a1d6-27fcacacde81",
            "compositeImage": {
                "id": "bf5c8f08-3f69-45f3-939e-16df72060a96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c02cdd13-d29f-4c4b-bbf1-cc2e476c0478",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f211c813-b5a2-4b2c-bf6a-3b05d3bca4a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c02cdd13-d29f-4c4b-bbf1-cc2e476c0478",
                    "LayerId": "b4f94f12-e47b-433e-9d07-66dc7e8038b8"
                }
            ]
        },
        {
            "id": "b95f3093-3b96-49ef-bf78-c855f3a808d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca2ec2b7-eed6-4d51-a1d6-27fcacacde81",
            "compositeImage": {
                "id": "48aee15d-d7f0-4606-9100-ea364b6f13ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b95f3093-3b96-49ef-bf78-c855f3a808d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "226279ef-d903-4ad0-8554-5b97f20b3832",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b95f3093-3b96-49ef-bf78-c855f3a808d0",
                    "LayerId": "b4f94f12-e47b-433e-9d07-66dc7e8038b8"
                }
            ]
        },
        {
            "id": "c2c500cc-9387-4730-9f1c-934c04bc2213",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca2ec2b7-eed6-4d51-a1d6-27fcacacde81",
            "compositeImage": {
                "id": "cf739667-eb44-40df-80a8-f7421ae71ab7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2c500cc-9387-4730-9f1c-934c04bc2213",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6845473c-93ed-455e-977f-6add8fb85187",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2c500cc-9387-4730-9f1c-934c04bc2213",
                    "LayerId": "b4f94f12-e47b-433e-9d07-66dc7e8038b8"
                }
            ]
        },
        {
            "id": "486188fd-ed6f-43a9-a8b6-3e6e4c61a4a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca2ec2b7-eed6-4d51-a1d6-27fcacacde81",
            "compositeImage": {
                "id": "b9749a88-d477-4fb9-a35c-2271960a5aaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "486188fd-ed6f-43a9-a8b6-3e6e4c61a4a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "406f4149-1ef7-4ff7-af71-e8cd628154b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "486188fd-ed6f-43a9-a8b6-3e6e4c61a4a4",
                    "LayerId": "b4f94f12-e47b-433e-9d07-66dc7e8038b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b4f94f12-e47b-433e-9d07-66dc7e8038b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca2ec2b7-eed6-4d51-a1d6-27fcacacde81",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}