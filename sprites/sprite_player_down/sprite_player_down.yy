{
    "id": "49cca263-18c4-4761-b850-b8d42979bb2b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "42a8b608-84cb-40ac-84e8-0e943920360d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49cca263-18c4-4761-b850-b8d42979bb2b",
            "compositeImage": {
                "id": "5e8bb460-aa5c-44e8-9ff1-ceef2e6c032a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42a8b608-84cb-40ac-84e8-0e943920360d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bed6d56f-e070-4b1e-bb41-04d3dd14c63c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42a8b608-84cb-40ac-84e8-0e943920360d",
                    "LayerId": "260c3f1a-f902-4e64-99be-188ab2d38003"
                }
            ]
        },
        {
            "id": "a3dbbda2-4d70-414b-b109-b4efef28b18f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49cca263-18c4-4761-b850-b8d42979bb2b",
            "compositeImage": {
                "id": "2d46e40b-7f5d-48cd-9500-5d3753d6796c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3dbbda2-4d70-414b-b109-b4efef28b18f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff84909c-8bc0-4414-9b9c-8ef7a79c5285",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3dbbda2-4d70-414b-b109-b4efef28b18f",
                    "LayerId": "260c3f1a-f902-4e64-99be-188ab2d38003"
                }
            ]
        },
        {
            "id": "d0b230ca-7622-4cf8-a50a-304cbc2c61bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49cca263-18c4-4761-b850-b8d42979bb2b",
            "compositeImage": {
                "id": "52d25885-5c47-4575-9e49-64b8fde03365",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0b230ca-7622-4cf8-a50a-304cbc2c61bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82d59538-5d90-429e-9b05-3ecde9e11edc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0b230ca-7622-4cf8-a50a-304cbc2c61bc",
                    "LayerId": "260c3f1a-f902-4e64-99be-188ab2d38003"
                }
            ]
        },
        {
            "id": "255a557f-250d-4799-bcbf-400bbbf6fab4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49cca263-18c4-4761-b850-b8d42979bb2b",
            "compositeImage": {
                "id": "b3a4d7ac-efff-482a-a785-ab2307bd7a03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "255a557f-250d-4799-bcbf-400bbbf6fab4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "772aacad-3d59-4073-9226-050d13e0996f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "255a557f-250d-4799-bcbf-400bbbf6fab4",
                    "LayerId": "260c3f1a-f902-4e64-99be-188ab2d38003"
                }
            ]
        },
        {
            "id": "a28bcc79-7514-4fb9-b3fc-27148e5244db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49cca263-18c4-4761-b850-b8d42979bb2b",
            "compositeImage": {
                "id": "121f17cc-bde8-4c46-a2a8-5ffbb8c41b61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a28bcc79-7514-4fb9-b3fc-27148e5244db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "841682ed-bb1e-4f96-8c87-138fcffbf485",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a28bcc79-7514-4fb9-b3fc-27148e5244db",
                    "LayerId": "260c3f1a-f902-4e64-99be-188ab2d38003"
                }
            ]
        },
        {
            "id": "37a82284-a4bb-46ba-8026-3f151d4cb382",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49cca263-18c4-4761-b850-b8d42979bb2b",
            "compositeImage": {
                "id": "3de5d132-e667-4924-a7e7-1e8e4f282f95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37a82284-a4bb-46ba-8026-3f151d4cb382",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c7b91ae-b06d-4316-9c83-fdbbe4ab3de5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37a82284-a4bb-46ba-8026-3f151d4cb382",
                    "LayerId": "260c3f1a-f902-4e64-99be-188ab2d38003"
                }
            ]
        },
        {
            "id": "da44eb67-0917-43d3-bbd5-968ac225a880",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49cca263-18c4-4761-b850-b8d42979bb2b",
            "compositeImage": {
                "id": "2d7409a7-cee8-4e46-82fb-55519eaada34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da44eb67-0917-43d3-bbd5-968ac225a880",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74118baf-02ee-4085-8547-7a4c2e045057",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da44eb67-0917-43d3-bbd5-968ac225a880",
                    "LayerId": "260c3f1a-f902-4e64-99be-188ab2d38003"
                }
            ]
        },
        {
            "id": "9d193eed-cce8-4650-9985-e4d2f4cca465",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49cca263-18c4-4761-b850-b8d42979bb2b",
            "compositeImage": {
                "id": "f0c9bcbf-9227-4ef4-9736-ce3b86550b27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d193eed-cce8-4650-9985-e4d2f4cca465",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa016fb6-b232-4156-a428-ae68115531e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d193eed-cce8-4650-9985-e4d2f4cca465",
                    "LayerId": "260c3f1a-f902-4e64-99be-188ab2d38003"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "260c3f1a-f902-4e64-99be-188ab2d38003",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49cca263-18c4-4761-b850-b8d42979bb2b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}