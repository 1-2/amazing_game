{
    "id": "1d3ab889-6fdd-42ea-baae-6e065fc50698",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a5c5a088-27f5-4294-a873-97faf21c9172",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d3ab889-6fdd-42ea-baae-6e065fc50698",
            "compositeImage": {
                "id": "e294c47a-36bd-44b8-b582-7f6c89340960",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5c5a088-27f5-4294-a873-97faf21c9172",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7948ab50-74c4-4b30-a088-d09f0ce66ed7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5c5a088-27f5-4294-a873-97faf21c9172",
                    "LayerId": "c457e9f3-b027-4b3b-a0a8-3614a6f037c0"
                }
            ]
        },
        {
            "id": "c96cd77a-a6d7-43f7-a7db-ae47f44a2106",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d3ab889-6fdd-42ea-baae-6e065fc50698",
            "compositeImage": {
                "id": "98094b6f-e29c-4f58-b724-3b9f566d5fcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c96cd77a-a6d7-43f7-a7db-ae47f44a2106",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a80b5e27-e686-46cf-a71b-7acf52ca23c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c96cd77a-a6d7-43f7-a7db-ae47f44a2106",
                    "LayerId": "c457e9f3-b027-4b3b-a0a8-3614a6f037c0"
                }
            ]
        },
        {
            "id": "7c05b5d5-a651-48b4-9a74-d91828c49b10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d3ab889-6fdd-42ea-baae-6e065fc50698",
            "compositeImage": {
                "id": "dd12d1ca-67f7-4aae-b4c0-342165f42c5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c05b5d5-a651-48b4-9a74-d91828c49b10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0234a8fa-a6eb-4655-a505-2f41f3356629",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c05b5d5-a651-48b4-9a74-d91828c49b10",
                    "LayerId": "c457e9f3-b027-4b3b-a0a8-3614a6f037c0"
                }
            ]
        },
        {
            "id": "f28fa679-b069-4238-860b-5b347e60afb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d3ab889-6fdd-42ea-baae-6e065fc50698",
            "compositeImage": {
                "id": "13092b19-bc56-483f-bfe3-8a632d00a31f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f28fa679-b069-4238-860b-5b347e60afb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c61da6fe-8304-490b-a611-320555ed1ccf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f28fa679-b069-4238-860b-5b347e60afb9",
                    "LayerId": "c457e9f3-b027-4b3b-a0a8-3614a6f037c0"
                }
            ]
        },
        {
            "id": "64200c1c-6f99-416b-87c0-6e9676460441",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d3ab889-6fdd-42ea-baae-6e065fc50698",
            "compositeImage": {
                "id": "1640910a-5e50-4a10-b5f3-2f549c613e8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64200c1c-6f99-416b-87c0-6e9676460441",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "295c30a2-ac01-40bb-9b13-b1a4b4288029",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64200c1c-6f99-416b-87c0-6e9676460441",
                    "LayerId": "c457e9f3-b027-4b3b-a0a8-3614a6f037c0"
                }
            ]
        },
        {
            "id": "9c0c829b-8ef4-470f-a3bc-1b1f7dcf9f2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d3ab889-6fdd-42ea-baae-6e065fc50698",
            "compositeImage": {
                "id": "ac70138e-c0c3-4577-ad45-5a235a51a264",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c0c829b-8ef4-470f-a3bc-1b1f7dcf9f2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12204741-278a-48cd-8b4f-4dc2c9c5232d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c0c829b-8ef4-470f-a3bc-1b1f7dcf9f2e",
                    "LayerId": "c457e9f3-b027-4b3b-a0a8-3614a6f037c0"
                }
            ]
        },
        {
            "id": "00623bf6-3906-4681-b0f7-7d684c020710",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d3ab889-6fdd-42ea-baae-6e065fc50698",
            "compositeImage": {
                "id": "b141e970-579c-4535-8651-8a35ce4fe4fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00623bf6-3906-4681-b0f7-7d684c020710",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a63c48c7-a83f-472b-ae1c-472a222b80dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00623bf6-3906-4681-b0f7-7d684c020710",
                    "LayerId": "c457e9f3-b027-4b3b-a0a8-3614a6f037c0"
                }
            ]
        },
        {
            "id": "929267c1-fc7d-44f5-9621-66a4aef4bf3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d3ab889-6fdd-42ea-baae-6e065fc50698",
            "compositeImage": {
                "id": "46ed94a7-aa15-4f12-a1a2-9b3fb2ad8359",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "929267c1-fc7d-44f5-9621-66a4aef4bf3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7d4bd11-5230-4d78-aa02-bddb22c19524",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "929267c1-fc7d-44f5-9621-66a4aef4bf3c",
                    "LayerId": "c457e9f3-b027-4b3b-a0a8-3614a6f037c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c457e9f3-b027-4b3b-a0a8-3614a6f037c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d3ab889-6fdd-42ea-baae-6e065fc50698",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}