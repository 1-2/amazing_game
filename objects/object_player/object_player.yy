{
    "id": "846062b7-e4b3-4e3c-91e0-e09b0d57ff21",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_player",
    "eventList": [
        {
            "id": "4388260e-3ed0-43d1-8c54-a744398e8f91",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "846062b7-e4b3-4e3c-91e0-e09b0d57ff21"
        },
        {
            "id": "42b9b371-2e07-445b-bb6f-6a961e3cbe15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 5,
            "m_owner": "846062b7-e4b3-4e3c-91e0-e09b0d57ff21"
        },
        {
            "id": "d7b6226c-b9dd-4492-9679-a28c565f67ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 5,
            "m_owner": "846062b7-e4b3-4e3c-91e0-e09b0d57ff21"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "49cca263-18c4-4761-b850-b8d42979bb2b",
    "visible": true
}