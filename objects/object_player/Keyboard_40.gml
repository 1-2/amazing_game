/// @DnDAction : YoYo Games.Instances.Set_Sprite
/// @DnDVersion : 1
/// @DnDHash : 4DCB81BC
/// @DnDArgument : "imageind" "image_index"
/// @DnDArgument : "spriteind" "sprite_player_down"
/// @DnDSaveInfo : "spriteind" "49cca263-18c4-4761-b850-b8d42979bb2b"
sprite_index = sprite_player_down;
image_index = image_index;

/// @DnDAction : YoYo Games.Instances.Sprite_Animation_Speed
/// @DnDVersion : 1
/// @DnDHash : 66F0CAA8
image_speed = 1;

/// @DnDAction : YoYo Games.Instances.Sprite_Scale
/// @DnDVersion : 1
/// @DnDHash : 39E335DB
image_xscale = 1;
image_yscale = 1;

/// @DnDAction : YoYo Games.Movement.Set_Direction_Fixed
/// @DnDVersion : 1.1
/// @DnDHash : 3A9B3105
/// @DnDArgument : "direction" "270"
direction = 270;

/// @DnDAction : YoYo Games.Movement.Set_Speed
/// @DnDVersion : 1
/// @DnDHash : 70E855C7
/// @DnDArgument : "speed" "4"
speed = 4;